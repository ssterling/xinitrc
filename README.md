# Seth's xinitrc

A simple drop-in `xinitrc` for most systems.  This repository includes some
example files in `xinitrc.d`, such as for calling up UIM or the XScreenSaver
daemon.  Enable these files by copying them to your system as necessary.

## To-do

* Select window manager via `$1` argument to `xinitrc`
* Possible WM-specific commands
* Better, easier variables/configuration

## Contributing

Use tab characters to indent and spaces to align.  UNIX line-endings.
Just submit a pull request to get your code in; nothing fancy.
